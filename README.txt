fastqCleaner.pl README.txt

author:       	Sean Gallaher
email:		gallaher@chem.ucla.edu
date:         	08-SEP-2016
version:      	v1.1

usage:
 to view the help:
   ./fastqCleaner.pl -h  

 to run on a fastq file: 
   ./fastqCleaner.pl mySequencFile.fq

 to run on the second file of a pair:
   ./fastqCleaner.pl -pe myEnd1.problems.txt myEnd2.fq

description:
	I recently downloaded some archived FASTQ files
	from SRA, and found that some of them were corrupt.
	A handful of lines had been converted into
	something that the text editor interpretted as
	binary. This caused all kinds of problems with
	my analysis. In the end, I wrote this script
	to strip out the bad reads. It checks each read
	to make sure it meets the following criterea:
	
	1)	Each read has a read name starting with “@”
	2) 	This is followed by a sequence line consisting 
		of only [ACGTNacgtn]
	3)	This is followed by line that starts with “+”
	4)	This is followed by a line with the same number 
		of characters as the sequence line. 

	After running, it generates a report to tell you if
	is rejected any lines. If it did, and you are using
	it on Paired End reads, you can run it on the other
	set of reads and it will remove the corresponding
	read pair. 

