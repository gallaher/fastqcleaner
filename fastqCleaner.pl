#!/usr/bin/perl

use strict;
use warnings;

# This is a script to go through a fastq file,
# and strip out any irregularities.
# It can also delete the corresponding read 
# pair when there are PE reads. 
# author:	Sean Gallaher
# date: 	16-AUG-2016
# version:	v1.1
# name:		fastqCleaner.pl


my $fileIn = shift @ARGV;
my $help = "\nThis script can be run on a fastq file\nand will pull out reads that \nappear to be defective.\nFor example, sequence reads that include \ncharacters other than \[ACGTNacgtn\]\nIt takes the name of a file ending in \n.fastq or .fq as input.\n\nOn one file, use:\ncleanFastq.pl myFile.fq\n\nTo run on the second file of a pair, use:\ncleanFastq.pl -pe myFirstFile.problems.txt mySecondFile.fq\n\n";

if (!defined $fileIn) {
	die "$help\n";
}
elsif ($fileIn eq "-h") {
	die "$help\n";
}

my %probReads;
my $filterProbReads = "no";

if ($fileIn eq "-pe") {
	my $probFile = shift @ARGV;
	$filterProbReads = "yes";
	$fileIn = shift @ARGV;
	open (PROBSET, "<", $probFile)
		or die "Cannot open problem file $probFile: $!\n";
	while (my $line = <PROBSET>) {
		chomp $line;
		if ($line eq "No problems observed.") {
			$filterProbReads = "no";
			print STDOUT "No problem reads to filter.\n\n";
		}
		elsif ($line =~ m/^problem sequence:(.+)/) {
			$probReads{$1}++;
		}
	}
	close (PROBSET);
}


$fileIn =~ m/(.+)\.(fq|fastq)/;
my $fileOut = $1 . ".clean." . $2;
my $problems = $1 . ".problems.txt";


open (IN, '<', $fileIn)
	or die "Cannot open $fileIn: $!\n";
open (OUT, '>', $fileOut)
	or die "Cannot open $fileOut: $!\n";
open (PROB, '>', $problems)
	or die "Cannot open $problems: $!\n";


my $i=1;
my $lineNumber = 1;
my $name;
my $seq = "blank";
my $qualName = "blank";
my $qual = "blank";
my $problemCount = 0;


while (my $line = <IN>) {
	chomp $line;
	if (!defined $name)  {
		if ($line =~ m/^\@SRR/) {
			$name = $line;
			$i++;
		}
		else {
			next;
		}	
	}
	elsif ($line =~ m/^\@SRR/) {
		if ($name =~ m/^\@SRR/) {
			if ($seq =~ m/^[ACGTNacgtn]+$/) {
				my $seqLen = length $seq ;
				my $qualLen = length $qual ;
				if ($qualName =~ m/^\+/) {
					if ($seqLen == $qualLen) {
						if ($filterProbReads eq "yes") {
							if (!defined $probReads{$name}) {
								print OUT "$name\n$seq\n+\n$qual\n";
							}
						}
						elsif ($filterProbReads eq "no") {
							print OUT "$name\n$seq\n+\n$qual\n";
						}

					}
					else {
						$problemCount++;
						print PROB "There is problem with the quality score on line $lineNumber: \n$qual\nproblem sequence:$name\n";
					}
				}
				else {
					$problemCount++;
					print PROB "There is a problem with the quality line name on line $lineNumber:\n$qualName\nproblem sequence:$name\n";
				}
			}
			else {
				$problemCount++;
				print PROB "There is a problem with the sequence line on line $lineNumber:\n$seq\nproblem sequence:$name\n";
			}
		}
		else {
			$problemCount++;
			print PROB "There is a problem with the read name on line $lineNumber:\n$name\nproblem sequence:$name\n";
		}
		$name = $line;
		$seq = "blank";
		$qualName = "blank";
		$qual = "blank";
		$i = 2;
	} 
	elsif ($i == 2) {
		$seq = $line;
		$i++;
	}
	elsif ($i == 3) {
		$qualName = $line;
		$i++;
	}
	elsif ($i == 4) {
		$qual = $line;
		$i++;
	}
	$lineNumber++;
}
if ($name =~ m/^\@SRR/) {
	if ($seq =~ m/^[ACGTNacgtn]+$/) {
		my $seqLen = length $seq ;
		my $qualLen = length $qual ;
		if ($qualName =~ m/^\+/) {
	        	if ($seqLen == $qualLen) {
				print OUT "$name\n$seq\n+\n$qual\n";
			}
                        else {
                        	$problemCount++;
                                print PROB "There is problem with the quality score on line $lineNumber: \n$qual\n";          
                        }
		}
                else {
                	$problemCount++;
                        print PROB "There is a problem with the quality line name on line $lineNumber:\n$qualName\n"; 
                }
	}
        else {
        	$problemCount++;
               	print PROB "There is a problem with the sequence line on line $lineNumber:\n$seq\n";          
	}
}
else {
	$problemCount++;
        print PROB "There is a problem with the read name on line $lineNumber:\n$name\n";
}


if ($problemCount == 0) {
	print PROB "No problems observed.\n";
}

close (IN);
close (OUT);
close (PROB);








